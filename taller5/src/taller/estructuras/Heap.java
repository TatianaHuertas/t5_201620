package taller.estructuras;

import java.util.Iterator;

public class Heap<T> implements IHeap<T>
{
	private Nodo<T> primero;
	private MaxHeap<T> maxHeap;

	public void add(T elemento) 
	{

	}

	public T peek() 
	{
		return null;
	}

	public T poll()
	{
		return null;
	}

	public int size()
	{
		return 0;
	}

	public boolean isEmpty()
	{
		return false;
	}

	public void siftUp() 
	{		
	}

	public void siftDown() 
	{		
	}

	/**
	 * Retorna un iterador de la pila 
	 * @return un iterador de la pila
	 */
	public Iterator<T> iterator() 
	{
		return new ListIterator(primero);
	}

	// Iterador de la pila
	private class ListIterator<T> implements Iterator<T> 
	{
		private Nodo<T> current;

		public ListIterator(Nodo<T> current) 
		{
			this.current = current;
		}

		public boolean hasNext() 
		{
			return current != null;
		}

		public void remove()
		{
			throw new UnsupportedOperationException();
		}

		public T next()
		{
			if (hasNext()) 
			{
				T item = current.item;
				current = current.siguiente;
			}
			return null;
		}
	}

	// Nodo de la pila
	private class Nodo<T> 
	{
		private T item;
		private Nodo siguiente;

		// Constructor
		public Nodo(T pItem) 
		{
			item = pItem;
		}

		public T getItem() 
		{
			return item;
		}

		public void setItem(T item) 
		{
			this.item = item;
		}

		public Nodo getNext() 
		{
			return siguiente;
		}

		public void setNext(Nodo siguiente)
		{
			this.siguiente = siguiente;
		}
	}
	
	private class MaxHeap<T> 
	{
		
	}
}